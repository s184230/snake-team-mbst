package resources.model;

import resources.Main;
import java.awt.*;
import java.util.ArrayList;

public class Snake {
    public Point head = new Point();
    public ArrayList<Point> body = new ArrayList<Point>();

    public Snake(int x, int y) {
        this.head.setLocation(x, y);
        this.body.add(new Point(x + 1, y));
    }

    public void move(char dir) {

        body.add(new Point(head));

        switch (dir) {
            case 'U':
                head.translate(0, -1);
                if (borderControl()) {
                    head.setLocation(head.getX(), Main.X_VALUE-1);
                }
                break;

            case 'D':
                head.translate(0, 1);
                if (borderControl()) {
                    head.setLocation(head.getX(), 0);
                }
                break;

            case 'L':
                head.translate(-1, 0);
                if (borderControl()) {
                    head.setLocation(Main.Y_VALUE -1, head.getY());
                }
                break;

            case 'R':
                head.translate(1, 0);
                if (borderControl()) {
                    head.setLocation(0, head.getY());
                }
                break;
        }
    }


    public boolean ateApple(Apple apple) {
        if (!this.head.equals(apple.getPos())) {
            this.body.remove(0);
        }
        return this.head.equals(apple.getPos());
    }

    private boolean borderControl() {
        return head.getX() < 0 || head.getY() < 0 || head.getX() >= Main.X_VALUE || head.getY() >= Main.Y_VALUE;
    }

    public boolean hitSelf() {
        // hvis head er i body, så er det no good
        return this.body.contains(head);
    }
}
