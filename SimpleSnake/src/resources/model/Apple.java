package resources.model;

import resources.Main;

import java.awt.*;
import java.util.Random;

public class Apple {
    private Point pos = new Point();

    public Apple() {
        Random rnd = new Random();

        int x = rnd.nextInt(Main.Y_VALUE);
        int y = rnd.nextInt(Main.X_VALUE);

        pos.setLocation(x, y);
    }

    public void newApple() {
        // TODO: tjek om position er på slangen
        Random rnd = new Random();

        int x = rnd.nextInt(Main.Y_VALUE);
        int y = rnd.nextInt(Main.X_VALUE);

        pos.setLocation(x, y);
    }

    public Point getPos() {
        return this.pos;
    }

}
