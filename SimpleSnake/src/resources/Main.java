package resources;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import resources.controllers.Controller;
import resources.model.Apple;
import resources.model.Snake;
import resources.views.View;


public class Main extends Application {
    //X, Y_Value styrer antallet af tern
    //MULTIPLIYER styrer størrelsen af tern
    public final static int X_VALUE = 10;
    public final static int Y_VALUE = 10;
    public final static int MULTIPLIER = 30;

    boolean movingLeft = true;
    boolean movingRight = false;
    boolean movingDown = false;
    boolean movingUp = false;

    private Pane root;
    public static Scene scene;

//    public Snake snek = new Snake(X_VALUE / 2, Y_VALUE / 2);
//    public Apple apple = new Apple();

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("gaem of snek");

        Snake snek = new Snake(X_VALUE / 2, Y_VALUE / 2);
        Apple apple = new Apple();
        View view = new View(snek,apple);
        Controller Controller = new Controller(snek,apple,view);

        root = new Pane();
        scene = new Scene(root, X_VALUE * MULTIPLIER, Y_VALUE * MULTIPLIER);


        //Laver Canvas for spillepladen. Størrelsen er den samme som canvas.
        Canvas gridCanvas = new Canvas(scene.getWidth(), scene.getHeight());
        GraphicsContext gc = gridCanvas.getGraphicsContext2D();
        view.drawGrid(apple, snek, gc);

        scene.setOnKeyPressed(event -> {
            switch (event.getCode()) {
                case RIGHT:
                    if (!movingLeft) {
                        snek.move('R');
                        if (snek.ateApple(apple)) {
                            apple.newApple();
                        }
                        if (snek.hitSelf()) System.exit(0);
                        view.drawGrid(apple, snek, view.gc);
                        movingLeft = false;
                        movingRight = true;
                        movingDown = false;
                        movingUp = false;
                    }
                    break;
                case DOWN:
                    System.out.println("hej");
                    if (!movingUp) {

                        snek.move('D');
                        if (snek.ateApple(apple)) {
                            apple.newApple();
                        }
                        if (snek.hitSelf()) System.exit(0);
                        view.drawGrid(apple, snek, view.gc);
                        movingLeft = false;
                        movingRight = false;
                        movingDown = true;
                        movingUp = false;
                    }
                    break;
                case LEFT:
                    if (!movingRight) {
                        snek.move('L');
                        if (snek.ateApple(apple)) {
                            apple.newApple();
                        }
                        if (snek.hitSelf()) System.exit(0);
                        view.drawGrid(apple, snek, view.gc);
                        movingLeft = true;
                        movingRight = false;
                        movingDown = false;
                        movingUp = false;
                    }
                    break;
                case UP:
                    if (!movingDown) {
                        snek.move('U');
                        if (snek.ateApple(apple)) {
                            apple.newApple();
                        }
                        if (snek.hitSelf()) System.exit(0);
                        view.drawGrid(apple, snek, view.gc);
                        movingLeft = false;
                        movingRight = false;
                        movingDown = false;
                        movingUp = true;
                    }
                    break;
            }
        });

        root.getChildren().addAll(gridCanvas);
        view.makeScene(scene);
        root.getChildren().addAll(View.gridCanvas);

        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }

}
