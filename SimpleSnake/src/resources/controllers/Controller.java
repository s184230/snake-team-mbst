package resources.controllers;

import javafx.scene.Scene;
import resources.Main;
import resources.model.Apple;
import resources.model.Snake;
import resources.views.View;

public class Controller {
    boolean movingLeft = true;
    boolean movingRight = false;
    boolean movingDown = false;
    boolean movingUp = false;
    private Snake snek;
    private Apple apple;
    private View view;

    public Controller(Snake snek, Apple apple, View view){
        this.snek = snek;
        this.apple = apple;
        this.view = view;

    }
}
